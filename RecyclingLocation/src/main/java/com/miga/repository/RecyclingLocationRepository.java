package com.miga.repository;

import com.miga.entity.RecyclingLocation;
import com.miga.repository.custom.RecyclingLocationRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecyclingLocationRepository extends JpaRepository<RecyclingLocation, Long>, RecyclingLocationRepositoryCustom {

}
