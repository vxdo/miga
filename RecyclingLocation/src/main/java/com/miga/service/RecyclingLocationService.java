package com.miga.service;

import com.miga.entity.RecyclingLocation;
import com.miga.request.LocationRequest;
import java.util.List;

public interface RecyclingLocationService {

  RecyclingLocation createLocation(LocationRequest request);

  List<RecyclingLocation> getLocations(LocationRequest request);
}
