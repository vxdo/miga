package com.miga.service.impl;

import com.miga.entity.RecyclingLocation;
import com.miga.repository.RecyclingLocationRepository;
import com.miga.request.LocationRequest;
import com.miga.service.RecyclingLocationService;
import com.miga.util.DateTimeUtils;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecyclingLocationServiceImpl implements RecyclingLocationService {

  @Autowired
  RecyclingLocationRepository recyclingLocationRepository;

  @Override
  public RecyclingLocation createLocation(LocationRequest request) {
    RecyclingLocation location = new RecyclingLocation();
    mapInfo(location, request);
    location.setCreatedDate(DateTimeUtils.nowTimestamp());
    return recyclingLocationRepository.save(location);
  }

  @Override
  public List<RecyclingLocation> getLocations(LocationRequest request) {
    return recyclingLocationRepository.findAll();
  }

  private void mapInfo(RecyclingLocation location, LocationRequest request) {
    if (StringUtils.isNotBlank(request.getName())) {
      location.setName(request.getName());
    }
    if (StringUtils.isNotBlank(request.getAddress())) {
      location.setAddress(request.getAddress());
    }
    if (request.getLatitude() != null) {
      location.setLatitude(request.getLatitude());
    }
    if (request.getLongitude() != null) {
      location.setLongitude(request.getLongitude());
    }
    if (StringUtils.isNotBlank(request.getType())) {
      location.setType(request.getType());
    }
    if (StringUtils.isNotBlank(request.getCollectingType())) {
      location.setCollectingType(request.getCollectingType());
    }
    if (StringUtils.isNotBlank(request.getNote())) {
      location.setNote(request.getNote());
    }
  }
}
