package com.miga.repository;

import com.miga.entity.SaleTicket;
import com.miga.repository.custom.SaleTicketRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaleTicketRepository extends JpaRepository<SaleTicket, Long>, SaleTicketRepositoryCustom {

}
