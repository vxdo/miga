package com.miga.service;

import com.miga.entity.SaleTicket;
import com.miga.request.SaleTicketRequest;
import java.util.List;

public interface SaleTicketService {

  SaleTicket createTicket(SaleTicketRequest request);

  List<SaleTicket> getTickets(SaleTicketRequest request);

  SaleTicket updateTicket(SaleTicketRequest request);
}
