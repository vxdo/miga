package com.miga.service.impl;

import com.miga.entity.SaleTicket;
import com.miga.repository.SaleTicketRepository;
import com.miga.repository.UserRepository;
import com.miga.request.SaleTicketRequest;
import com.miga.service.ConnectingUserService;
import com.miga.service.SaleTicketService;
import com.miga.util.DateTimeUtils;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class SaleTicketServiceImpl implements SaleTicketService {

  @Autowired
  SaleTicketRepository saleTicketRepository;

  @Autowired
  ConnectingUserService connectingUserService;

  @Autowired
  UserRepository userRepository;

  @Override
  public SaleTicket createTicket(SaleTicketRequest request) {
    SaleTicket ticket = new SaleTicket();
    mapInfo(ticket, request);
    return saleTicketRepository.save(ticket);
  }

  @Override
  public SaleTicket updateTicket(SaleTicketRequest request) {
    Optional<SaleTicket> ticket = saleTicketRepository.findById(request.getSaleTicketId());
    if (ticket.isPresent()) {
      SaleTicket saleTicket = ticket.get();
      mapInfo(saleTicket, request);
      return saleTicketRepository.save(saleTicket);
    } else {
      throw new IllegalArgumentException("Ticket Id does not exist");
    }
  }

  @Override
  public List<SaleTicket> getTickets(SaleTicketRequest request) {
    Pageable pageable = PageRequest.of(Math.toIntExact(request.getOffset() / request.getLimit()), Math.toIntExact(request.getLimit()));
    return saleTicketRepository.findAll(pageable).getContent();
  }

  private void mapInfo(SaleTicket ticket, SaleTicketRequest request) {
    String fullName = userRepository.findByUserId(connectingUserService.getConnectingUser().getUserId()).getFullName();
    ticket.setCreatedDate(DateTimeUtils.nowTimestamp());
    ticket.setModifiedDate(DateTimeUtils.nowTimestamp());
    ticket.setCreatedBy(fullName);
    ticket.setModifiedBy(fullName);

    if (StringUtils.isNotBlank(request.getProductName())) {
      ticket.setProductName(request.getProductName());
    }
    if (request.getPrice() != null && request.getPrice().compareTo(BigDecimal.ZERO) < 0) {
      ticket.setPrice(request.getPrice());
    } else {
      throw new IllegalArgumentException("ticket price cannot be negative");
    }
    if (StringUtils.isNotBlank(request.getDescription())) {
      ticket.setDescription(request.getDescription());
    }
    if (StringUtils.isNotBlank(request.getStatus())) {
      ticket.setStatus(request.getStatus());
    }
  }
}
