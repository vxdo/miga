package com.miga.business;

import com.miga.common.request.NewsTemplateRequest;
import com.miga.result.newsTemplate.NewsTemplateDTO;
import java.util.List;

public interface NewsTemplateBusiness {

  NewsTemplateDTO createNewsTemplate(NewsTemplateRequest request);

  List<NewsTemplateDTO> getNewsTemplate(NewsTemplateRequest request);
}
