package com.miga.business;

import com.miga.request.LocationRequest;
import com.miga.result.recyclingLocation.LocationDTO;
import java.util.List;

public interface RecyclingLocationBusiness {

  List<LocationDTO> getLocations(LocationRequest request);

  LocationDTO createLocation(LocationRequest request);
}
