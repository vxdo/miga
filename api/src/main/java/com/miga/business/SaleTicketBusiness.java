package com.miga.business;

import com.miga.request.SaleTicketRequest;
import com.miga.result.SaleTicket.SaleTicketDTO;
import java.util.List;

public interface SaleTicketBusiness {

  SaleTicketDTO createTicket(SaleTicketRequest request);

  List<SaleTicketDTO> search(SaleTicketRequest request);

  SaleTicketDTO updateTicket(SaleTicketRequest request);
}
