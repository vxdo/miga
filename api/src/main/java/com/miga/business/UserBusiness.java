package com.miga.business;

import com.miga.entity.User;
import com.miga.request.UserRequest;
import com.miga.result.profile.UserDTO;
import org.springframework.web.multipart.MultipartFile;

public interface UserBusiness {

  UserDTO save(User user);

  UserDTO findUserByUserId(Long userId);

  UserDTO register(UserRequest request, MultipartFile file) throws Exception;

  UserDTO login();

  UserDTO logout();

  UserDTO resetPassword(String email);

  UserDTO changePassword();
}
