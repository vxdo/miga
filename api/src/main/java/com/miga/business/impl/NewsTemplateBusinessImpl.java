package com.miga.business.impl;

import com.miga.business.NewsTemplateBusiness;
import com.miga.common.request.NewsTemplateRequest;
import com.miga.mapper.NewsTemplateMapper;
import com.miga.result.newsTemplate.NewsTemplateDTO;
import com.miga.service.NewsTemplateService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NewsTemplateBusinessImpl implements NewsTemplateBusiness {

  @Autowired
  NewsTemplateService newsTemplateService;

  public NewsTemplateDTO createNewsTemplate(NewsTemplateRequest request) {
    return NewsTemplateMapper.INSTANCE.toNewsTemplateDTO(newsTemplateService.createNewsTemplate(request));
  }

  public List<NewsTemplateDTO> getNewsTemplate(NewsTemplateRequest request) {
    return NewsTemplateMapper.INSTANCE.toNewsTemplateDTOs(newsTemplateService.getNewsTemplate(request));
  }
}
