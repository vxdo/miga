package com.miga.business.impl;

import com.miga.business.RecyclingLocationBusiness;
import com.miga.mapper.LocationMapper;
import com.miga.request.LocationRequest;
import com.miga.result.recyclingLocation.LocationDTO;
import com.miga.service.RecyclingLocationService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecyclingLocationBusinessImpl implements RecyclingLocationBusiness {

  @Autowired
  RecyclingLocationService recyclingLocationService;

  @Override
  public List<LocationDTO> getLocations(LocationRequest request) {
    return LocationMapper.INSTANCE.toLocationDTOs(recyclingLocationService.getLocations(request));
  }

  @Override
  public LocationDTO createLocation(LocationRequest request) {
    return LocationMapper.INSTANCE.toLocationDTO(recyclingLocationService.createLocation(request));
  }
}
