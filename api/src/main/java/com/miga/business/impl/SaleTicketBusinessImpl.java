package com.miga.business.impl;

import com.miga.business.SaleTicketBusiness;
import com.miga.mapper.SaleTicketMapper;
import com.miga.request.SaleTicketRequest;
import com.miga.result.SaleTicket.SaleTicketDTO;
import com.miga.service.SaleTicketService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SaleTicketBusinessImpl implements SaleTicketBusiness {

  @Autowired
  SaleTicketService saleTicketService;

  @Override
  public SaleTicketDTO createTicket(SaleTicketRequest request) {
    return SaleTicketMapper.INSTANCE.toSaleTicketDTO(saleTicketService.createTicket(request));
  }

  @Override
  public List<SaleTicketDTO> search(SaleTicketRequest request) {
    return SaleTicketMapper.INSTANCE.toSaleTicketDTOs(saleTicketService.getTickets(request));
  }

  @Override
  public SaleTicketDTO updateTicket(SaleTicketRequest request) {
    return SaleTicketMapper.INSTANCE.toSaleTicketDTO(saleTicketService.updateTicket(request));
  }
}
