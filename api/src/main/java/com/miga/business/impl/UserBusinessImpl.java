package com.miga.business.impl;

import com.miga.business.UserBusiness;
import com.miga.connector.StorageConnector;
import com.miga.entity.User;
import com.miga.enumeration.profile.AttachmentType;
import com.miga.mapper.UserMapper;
import com.miga.request.UserRequest;
import com.miga.result.profile.UploadFileRequest;
import com.miga.result.profile.UserDTO;
import com.miga.service.UserService;
import java.io.File;
import java.io.IOException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UserBusinessImpl implements UserBusiness {

  @Autowired
  UserService userService;

  @Autowired
  Environment environment;

  @Autowired
  StorageConnector storageConnector;

  @Override
  public UserDTO save(User user) {
    return UserMapper.INSTANCE.toUserDTO(userService.save(user));
  }

  @Override
  public UserDTO findUserByUserId(Long userId) {
    return UserMapper.INSTANCE.toUserDTO(userService.findByUserId(userId));
  }

  @Override
  public UserDTO register(UserRequest request, MultipartFile avatar) throws Exception {
    User user;
    try {
      user = userService.register(request);
    } catch (ConstraintViolationException | DataIntegrityViolationException e) {
      String error = e.getCause().getCause().getMessage();
      error = error.split("Detail: Key ")[1];
      error = error.replaceAll("[()]", "");
      throw new IllegalArgumentException(error);
    }
    if (avatar == null) {
      return UserMapper.INSTANCE.toUserDTO(user);
    }
    UploadFileRequest uploadFileRequest = new UploadFileRequest();
    convertToUploadFileRequest(avatar, uploadFileRequest, AttachmentType.AVATAR.getFolder());
    return UserMapper.INSTANCE.toUserDTO(createAvatar(uploadFileRequest, environment.getProperty("storage.rootPath"), user));
  }

  @Override
  public UserDTO login() {
    return null;
  }

  @Override
  public UserDTO logout() {
    return null;
  }

  @Override
  public UserDTO resetPassword(String email) {
    return null;
  }

  @Override
  public UserDTO changePassword() {
    return null;
  }

  private void convertToUploadFileRequest(MultipartFile file, UploadFileRequest uploadFileRequest, String fileType) throws IOException {
    uploadFileRequest.setFileName(file.getOriginalFilename());
    uploadFileRequest.setFileType(fileType);
    uploadFileRequest.setFileExtensionType(file.getContentType());
    uploadFileRequest.setFileInputStream(file.getInputStream());
  }

  private User createAvatar(UploadFileRequest fileUploaded, String rootPath, User user) throws Exception {
    String directoryPath = buildFilePath(rootPath, user.getUserId().toString(), fileUploaded.getFileType());
    storageConnector.cleanDirectories(directoryPath);
    String fileName = storageConnector.createFile(fileUploaded.getFileInputStream(), directoryPath, fileUploaded.getFileName());
    user.setAvatar(directoryPath + File.separator + fileName);
    return userService.save(user);
  }

  private String buildFilePath(String... args) {
    StringBuilder filePath = new StringBuilder();
    String prefix = "";
    for (String arg : args) {
      filePath.append(prefix);
      prefix = File.separator;
      filePath.append(arg);
    }
    return filePath.toString();
  }
}
