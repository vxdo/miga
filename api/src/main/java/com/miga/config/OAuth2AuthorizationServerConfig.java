package com.miga.config;

import com.miga.config.custom.auth.AccountkitTokenGranter;
import com.miga.config.custom.auth.EmailTokenGranter;
import com.miga.config.custom.auth.FacebookTokenGranter;
import com.miga.config.custom.auth.OTPTokenGranter;
import com.miga.config.custom.auth.UsernamePasswordTokenGranter;
import java.util.Arrays;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configuration.ClientDetailsServiceConfiguration;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

/**
 * Created by chautn on 7/15/2017.
 */
@Configuration
@EnableAuthorizationServer
public class OAuth2AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

  @Qualifier("dataSource")
  @Autowired
  private DataSource dataSource;

  @Qualifier("authenticationManagerBean")
  @Autowired
  private AuthenticationManager authenticationManager;

  @Override
  public void configure(final ClientDetailsServiceConfigurer clients) throws Exception {
    clients.withClientDetails(clientDetailsServices());
  }

  @Override
  public void configure(final AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
    oauthServer.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
  }

  @Override
  public void configure(final AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
    endpoints.tokenServices(tokenServices1()).setClientDetailsService(clientDetailsServices());
    OTPTokenGranter optTokenGranter = new OTPTokenGranter(
        authenticationManager,
        endpoints.getTokenServices(),
        endpoints.getClientDetailsService(),
        endpoints.getOAuth2RequestFactory());
    FacebookTokenGranter facebookTokenGranter = new FacebookTokenGranter(
        authenticationManager,
        endpoints.getTokenServices(),
        endpoints.getClientDetailsService(),
        endpoints.getOAuth2RequestFactory());
    AccountkitTokenGranter accountkitTokenGranter = new AccountkitTokenGranter(
        authenticationManager,
        endpoints.getTokenServices(),
        endpoints.getClientDetailsService(),
        endpoints.getOAuth2RequestFactory());
    UsernamePasswordTokenGranter passwordTokenGranter = new UsernamePasswordTokenGranter(
        authenticationManager,
        endpoints.getTokenServices(),
        endpoints.getClientDetailsService(),
        endpoints.getOAuth2RequestFactory()
    );
    EmailTokenGranter emailTokenGranter = new EmailTokenGranter(
        authenticationManager,
        endpoints.getTokenServices(),
        endpoints.getClientDetailsService(),
        endpoints.getOAuth2RequestFactory()
    );

    TokenGranter tokenGranter = new CompositeTokenGranter(
        Arrays.asList(optTokenGranter, facebookTokenGranter, accountkitTokenGranter, passwordTokenGranter, emailTokenGranter));

    endpoints.tokenStore(new JdbcTokenStore(dataSource))
        .authenticationManager(authenticationManager)
        .tokenGranter(tokenGranter);
  }

  @Bean
  @Primary
  public DefaultTokenServices tokenServices1() {
    final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
    defaultTokenServices.setTokenStore(tokenStore());
    defaultTokenServices.setSupportRefreshToken(true);
    defaultTokenServices.setAccessTokenValiditySeconds(0);
    return defaultTokenServices;
  }

  @Bean
  public ClientDetailsService clientDetailsServices() throws Exception {
    ClientDetailsServiceConfiguration serviceConfig = new ClientDetailsServiceConfiguration();
    serviceConfig.clientDetailsServiceConfigurer().jdbc(dataSource);
    return serviceConfig.clientDetailsService();
  }

  @Bean
  public TokenStore tokenStore() {
    return new JdbcTokenStore(dataSource);
  }

}
