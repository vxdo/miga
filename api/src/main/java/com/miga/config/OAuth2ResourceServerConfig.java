package com.miga.config;

import com.miga.config.custom.CustomOAuth2AuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

/**
 * Created by chautn on 7/19/2017.
 */
@EnableResourceServer
@Configuration
public class OAuth2ResourceServerConfig extends ResourceServerConfigurerAdapter {


  @Autowired
  private Environment environment;

  @Override
  public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
    resources.authenticationEntryPoint(new CustomOAuth2AuthenticationEntryPoint());
  }

  @Override
  public void configure(HttpSecurity http) throws Exception {
    ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry authorization = http.authorizeRequests();

    http.headers().frameOptions().disable();
    authorization.antMatchers("/", "/index", "/otp", "/version", "/version/**").permitAll()
        .antMatchers(HttpMethod.POST, "/news-template").permitAll()
        .antMatchers(HttpMethod.GET, "/news-template").permitAll()
        .antMatchers(HttpMethod.POST, "/recycling-location").permitAll()
        .antMatchers(HttpMethod.GET, "/recycling-location").permitAll()
        .antMatchers(HttpMethod.POST, "/user/**").permitAll()
        .antMatchers(HttpMethod.GET, "/sale-ticket").permitAll()
        .antMatchers("/**").authenticated();

  }
}
