package com.miga.config.custom;

import com.miga.constant.ApplicationConstant;
import com.miga.entity.CustomUserDetails;
import com.miga.entity.User;
import com.miga.entity.security.GrantedFunction;
import com.miga.enumeration.profile.UserStatus;
import com.miga.enumeration.profile.UserType;
import com.miga.service.UserService;
import com.miga.util.PasswordUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/**
 * Created by chautn on 7/20/2017.
 */
public class CustomAuthenticationProvider implements AuthenticationProvider {

  @Autowired
  UserService userService;

//  @Override
//  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//    return null;
//  }

  @Override
  public boolean supports(Class<?> authentication) {
    return
//        authentication.equals(OTPAuthenticationToken.class)
//        || authentication.equals(FacebookAuthenticationToken.class)
//        || authentication.equals(AccountkitAuthenticationToken.class)
        authentication.equals(UsernamePasswordAuthenticationToken.class);
//        || authentication.equals(EmailAuthenticationToken.class);
  }

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//    if (authentication instanceof OTPAuthenticationToken) {
//      return authenticateOTP((OTPAuthenticationToken) authentication);
//    } else if (authentication instanceof FacebookAuthenticationToken) {
//      return authenticationFacebook((FacebookAuthenticationToken) authentication);
//    } else if (authentication instanceof AccountkitAuthenticationToken) {
//      return authenticateAccountKit((AccountkitAuthenticationToken) authentication);
    if (authentication instanceof UsernamePasswordAuthenticationToken) {
      return authenticationUsernamePassword((UsernamePasswordAuthenticationToken) authentication);
    }
//    } else if (authentication instanceof EmailAuthenticationToken) {
//      return authenticationEmail((EmailAuthenticationToken) authentication);
//    } else {
    return null;
  }

//}

  //  private Authentication authenticationEmail(EmailAuthenticationToken authentication) {
//    Map<String, String> parameters = (Map<String, String>) authentication.getDetails();
//    String email = parameters.get("email");
//    String password = parameters.get("password");
//    UserType userType = UserType.getUserTypeByClient(parameters.get("client_id"));
//    if (userType != null && userType.isBaseGroup()) {
//      User user = userService.findMUserByEmailAndPassword(email, PasswordUtils.md5(password));
//      User userTempPassword = mUserService.findMUserByEmailAndTempPassword(email, PasswordUtils.md5(password));
//      if (userTempPassword != null && user == null) {
//        user = userTempPassword;
//      }
//
//      if (user != null) {
//        if (!UserStatus.ACTIVE.name().equals(user.getStatus())) {
//          throw new BadCredentialsException("Inactive user");
//        }
//        initPrincipal(authentication, user, userType);
//        return new EmailAuthenticationToken(authentication.getPrincipal(), null, getGrantedFunctions(permissionService.getUserFunctions(user.getUserId())));
//      } else {
//        throw new BadCredentialsException("Invalid email or password");
//      }
//    } else {
//      throw new BadCredentialsException("Unsupported client id");
//    }
//  }
//
  private Authentication authenticationUsernamePassword(UsernamePasswordAuthenticationToken authentication) {
    Map<String, String> parameters = (Map<String, String>) authentication.getDetails();
    String username = parameters.get("username");
    String phone = parameters.get("phone");
    String password = parameters.get("password");

    UserType userType = UserType.getUserTypeByClient(parameters.get("client_id"));
    if (password == null) {
      throw new IllegalArgumentException("missing password");
    }
    if (userType != null && userType.isBaseGroup()) {
      User user = null;
      if (parameters.containsKey("phone")) {
        if (StringUtils.isNotEmpty(phone)) {
          user = userService.findByPhone(phone);
        }
        if (user == null) {
          throw new BadCredentialsException("phone incorrect");
        }
      } else if (parameters.containsKey("username")) {
        if (StringUtils.isNotEmpty(username)) {
          user = userService.findByUsername(username);
        }
        if (user == null) {
          throw new BadCredentialsException("username incorrect");
        }
      } else {
        throw new BadCredentialsException("missing params");
      }

      // validate not login with master password
      if (!password.equals(ApplicationConstant.SECRET_KEY)) {
        // validate user password or tempPassword not empty
        if (StringUtils.isEmpty(user.getPassword())) {
          throw new BadCredentialsException("password does not exist");
        } else {
          // validate password not correct with current password
          boolean isSuccess = false;
          if (StringUtils.isNotEmpty(user.getPassword()) && PasswordUtils.checkPassword(password, user.getPassword())) {
            isSuccess = true;
          }
          // validate password not correct with temp password
          if (!isSuccess && StringUtils.isNotEmpty(user.getTempPassword()) && PasswordUtils.checkPassword(user.getTempPassword(), user.getPassword())) {
            isSuccess = true;
          }
          if (!isSuccess) {
            throw new BadCredentialsException("password incorrect");
          }
        }
      }

      if (!UserStatus.ACTIVE.name().equals(user.getStatus())) {
        throw new BadCredentialsException("user inactive");
      }
      initPrincipal(authentication, user, userType);
      return new UsernamePasswordAuthenticationToken(authentication.getPrincipal(), null,
          getGrantedFunctions(Collections.singleton("cms")));
    } else {
      throw new BadCredentialsException("client_id not supported");
    }
  }

  //
//  private Authentication authenticationFacebook(FacebookAuthenticationToken authentication) {
//    Map<String, String> parameters = (Map<String, String>) authentication.getDetails();
//    String facebookAccessToken = parameters.get(ApplicationConstant.TOKEN);
//    UserType userType = UserType.getUserTypeByClient(parameters.get(ApplicationConstant.CLIENT_ID));
//    if (userType != null && userType.isBaseGroup()) {
//      Facebook facebook = new FacebookTemplate(facebookAccessToken);
////      User facebookUser = facebook.userOperations().getUserProfile();
//      User facebookUser = facebook.fetchObject("me", User.class);
//      if (facebookUser != null) {
//        MUser user = mUserService.findMUserByFacebookId(facebookUser.getId());
//        if (user == null) {
//          user = mUserService.createMUserByFacebookId(facebookUser.getId());
//        }
//        if (!UserStatus.ACTIVE.name().equals(user.getStatus())) {
//          throw new BadCredentialsException("Inactive user");
//        }
//        initPrincipal(authentication, user, userType);
//        return new FacebookAuthenticationToken(authentication.getPrincipal(), null, getGrantedFunctions(permissionService.getUserFunctions(user.getUserId())));
//      } else {
//        throw new BadCredentialsException("Invalid facebook access token");
//      }
//    } else {
//      throw new BadCredentialsException("Unsupported client id");
//    }
//  }
//
//  private Authentication authenticateOTP(OTPAuthenticationToken authentication) throws AuthenticationException {
//    Map<String, String> parameters = (Map<String, String>) authentication.getDetails();
//    String otpNumber = parameters.get(ApplicationConstant.OTP);
//    String uuid = parameters.get(ApplicationConstant.TOKEN);
//    UserType userType = UserType.getUserTypeByClient(parameters.get(ApplicationConstant.CLIENT_ID));
//    if (userType != null && userType.isBaseGroup()) {
//      Otp otp = otpService.validateOtp(otpNumber, uuid);
//      if (otp != null) {
//        MUser user = mUserService.findMUserByPhone(otp.getPhoneNumber());
//        if (user == null) {
//          user = mUserService.createMUserByPhoneNumber(otp.getPhoneNumber());
//        }
//        if (!UserStatus.ACTIVE.name().equals(user.getStatus())) {
//          throw new BadCredentialsException("Inactive user");
//        }
//        initPrincipal(authentication, user, userType);
//        return new OTPAuthenticationToken(authentication.getPrincipal(), null, getGrantedFunctions(permissionService.getUserFunctions(user.getUserId())));
//      } else {
//        throw new BadCredentialsException("Invalid otp number");
//      }
//    } else {
//      throw new BadCredentialsException("Unsupported client id");
//    }
//  }
//
//  private Authentication authenticateAccountKit(AccountkitAuthenticationToken authentication) throws AuthenticationException {
//    Map<String, String> parameters = (Map<String, String>) authentication.getDetails();
//    String accountkitAccessToken = parameters.get(ApplicationConstant.TOKEN);
//    UserType userType = UserType.getUserTypeByClient(parameters.get(ApplicationConstant.CLIENT_ID));
//    if (userType != null && userType.isBaseGroup()) {
//      AccountkitTemplate accountkit = new AccountkitTemplate(accountkitAccessToken);
//      AccountkitProfile akProfile = accountkit.getAccountkitProfile();
//      if (akProfile != null && akProfile.getPhone() != null && StringUtils.isNotBlank(akProfile.getPhone().getPhoneNumber())) {
//        String phoneNumber = akProfile.getPhone().getNumber();
//        if (phoneNumber.charAt(0) == '+') {
//          phoneNumber = phoneNumber.substring(1);
//        }
//        MUser user = mUserService.findMUserByPhone(phoneNumber);
//        if (user == null) {
//          user = mUserService.createMUserByPhoneNumber(phoneNumber);
//        }
//        if (!UserStatus.ACTIVE.name().equals(user.getStatus())) {
//          throw new BadCredentialsException("Inactive user");
//        }
//        initPrincipal(authentication, user, userType);
//        return new AccountkitAuthenticationToken(authentication.getPrincipal(), null,
//            getGrantedFunctions(permissionService.getUserFunctions(user.getUserId())));
//      } else {
//        throw new BadCredentialsException("Invalid accountkit access token");
//      }
//    } else {
//      throw new BadCredentialsException("Unsupported client id");
//    }
//  }
//
  private String initActorId(User user, UserType userType) {
    if (UserType.CMS.equals(userType)) {
      return "userId: " + user.getUserId();
    } else {
      return null;
    }
  }

  private void initPrincipal(Authentication authentication, User user, UserType userType) {
    Map<String, String> parameters = (Map<String, String>) authentication.getDetails();

    CustomUserDetails principal = (CustomUserDetails) authentication.getPrincipal();
    principal.setUserType(userType);
    principal.setUserId(user.getUserId());
    principal.setActorId(initActorId(user, userType));
//    principal.setDeviceToken(parameters.get(ApplicationConstant.DEVICE_CODE));
//
//    if (StringUtils.isNotBlank(parameters.get(ApplicationConstant.DEVICE_CODE))) {
//      DeviceRequest deviceRequest = new DeviceRequest();
//      deviceRequest.setDeviceCode(parameters.get(ApplicationConstant.DEVICE_CODE));
//      deviceRequest.setDeviceType(parameters.get(ApplicationConstant.DEVICE_TYPE));
//      deviceRequest.setLanguage(parameters.get(ApplicationConstant.LANGUAGE));
//      deviceService.register(user, principal, deviceRequest);
//    }
//
//    if (userType.isBaseGroup()) {
//      Set<String> groups = permissionService.getUserGroups(user.getUserId());
//      if (!groups.contains(userType.name())) {
//        permissionService.addUserToGroup(user.getUserId(), userType.name());
//      }
//    }
  }

  private List<GrantedFunction> getGrantedFunctions(Set<String> functions) {
    List<GrantedFunction> result = new ArrayList<>();
    for (String function : functions) {
      result.add(new GrantedFunction(function));
    }
    return result;
  }
}
