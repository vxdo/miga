package com.miga.config.custom;

import com.miga.result.RestResult;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.error.DefaultOAuth2ExceptionRenderer;
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.error.OAuth2ExceptionRenderer;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

/**
 * Created by chautn on 7/22/2017.
 */
public class CustomOAuth2SecurityExceptionHandler {

  private WebResponseExceptionTranslator exceptionTranslator = new DefaultWebResponseExceptionTranslator();
  private OAuth2ExceptionRenderer exceptionRenderer = new DefaultOAuth2ExceptionRenderer();
  private HandlerExceptionResolver handlerExceptionResolver = new DefaultHandlerExceptionResolver();


  final void doHandle(HttpServletRequest request, HttpServletResponse response, Exception authException)
      throws IOException, ServletException {
    try {
      ResponseEntity<OAuth2Exception> result = exceptionTranslator.translate(authException);
      ResponseEntity<RestResult> restResult = enhanceResponse(result, authException);
      exceptionRenderer.handleHttpEntityResponse(restResult, new ServletWebRequest(request, response));
      response.flushBuffer();
    } catch (ServletException e) {
      if (handlerExceptionResolver.resolveException(request, response, this, e) == null) {
        throw e;
      }
    } catch (IOException | RuntimeException e) {
      throw e;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private ResponseEntity<RestResult> enhanceResponse(ResponseEntity<OAuth2Exception> result, Exception authException) {
    RestResult restResult = new RestResult();
    restResult.fail(authException.getMessage());
    return ResponseEntity.status(result.getStatusCode()).headers(result.getHeaders()).body(restResult);
  }
}
