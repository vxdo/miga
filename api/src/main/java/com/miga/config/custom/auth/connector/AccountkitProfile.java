package com.miga.config.custom.auth.connector;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountkitProfile {

//  {
//    "id": "628593457473088",
//    "phone": {
//       "number": "+84948459484",
//       "country_prefix": "84",
//       "national_number": "948459484"
//    },
//    "application": {
//       "id": "126144691582482"
//    }
// }

  private String id;
  private Phone phone;
  private Application application;

  private String accessToken;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Phone getPhone() {
    return phone;
  }

  public void setPhone(Phone phone) {
    this.phone = phone;
  }

  public Application getApplication() {
    return application;
  }

  public void setApplication(Application application) {
    this.application = application;
  }


  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }


  public static class Phone {

    private String number;
    @JsonProperty("country_prefix")
    private String countryPrefix;
    @JsonProperty("national_number")
    private String nationalNumber;

    public String getNumber() {
      return number;
    }

    public void setNumber(String number) {
      this.number = number;
    }

    public String getCountryPrefix() {
      return countryPrefix;
    }

    public void setCountryPrefix(String countryPrefix) {
      this.countryPrefix = countryPrefix;
    }

    public String getNationalNumber() {
      return nationalNumber;
    }

    public void setNationalNumber(String nationalNumber) {
      this.nationalNumber = nationalNumber;
    }

    public String getPhoneNumber() {
      return getCountryPrefix() + getNationalNumber();
    }
  }

  public static class Application {

    private String id;

    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }
  }


}
