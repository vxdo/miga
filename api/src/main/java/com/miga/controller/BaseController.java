package com.miga.controller;

import com.miga.exception.ApacheClientException;
import com.miga.exception.CommonException;
import com.miga.exception.CustomInvalidParameterException;
import com.miga.exception.PaymentException;
import com.miga.exception.PaymentInvalidParameterException;
import com.miga.result.RestResult;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;

/**
 * Common method for handle exception
 * <p>
 * BaseController.java
 *
 * @author VuLe
 * @date Apr 19, 2017
 */
public class BaseController {

//  @Autowired
//  ResourceBundle resourceBundle;

  /**
   * Return http status 400 - Invalid request params
   */
  @ExceptionHandler({CustomInvalidParameterException.class})
  @ResponseBody
  public ResponseEntity<RestResult> handleInvalidParameterException(CustomInvalidParameterException exception) {
    RestResult restResult = new RestResult();
    restResult.setStatus(RestResult.STATUS_ERROR);
    if (CollectionUtils.isEmpty(exception.getMessages())) {
      restResult.setMessage(exception.getMessage());
    } else {
      restResult.setMessages(exception.getMessages());
    }
    return new ResponseEntity<>(restResult, HttpStatus.BAD_REQUEST);
  }

  /**
   * Return http status 401 - UNAUTHORIZED
   */
  @ExceptionHandler({OAuth2Exception.class})
  @ResponseBody
  public ResponseEntity<RestResult<Object>> handleUnauthorizedException(OAuth2Exception exception) {
    List<String> exceptions = getRecMsgErrors(exception, null);
    return createExceptionResponse(HttpStatus.UNAUTHORIZED, Collections.singletonList(exception.getMessage()), exceptions);
  }

  /**
   * Return Http status Error from API request
   */
  @ExceptionHandler({ApacheClientException.class})
  @ResponseBody
  public ResponseEntity<RestResult<Object>> handleApacheClientException(ApacheClientException exception) {
    List<String> exceptions = getRecMsgErrors(exception, null);
    return createExceptionResponse(HttpStatus.valueOf(exception.getCode()), Collections.singletonList(exception.getMessage()), exceptions);
  }

  /**
   * Return http status 403 - FORBIDDEN
   */
  @ExceptionHandler({AccessDeniedException.class})
  @ResponseBody
  public ResponseEntity<RestResult<Object>> handleAccessDeniedException(AccessDeniedException exception, HttpServletRequest request) {
    List<String> exceptions = getRecMsgErrors(exception, null);
    return createExceptionResponse(HttpStatus.FORBIDDEN, Collections.singletonList("Forbidden: You dont have permission."), exceptions);
  }

  /**
   * Handler business convention exception : not exist or not accepted data Return http status 400 - BAD REQUEST
   */
  @ExceptionHandler({CommonException.class})
  @ResponseBody
  public ResponseEntity<RestResult<? extends Void>> handleCommonException(CommonException exception, HttpServletRequest request) {

    return new ResponseEntity<RestResult<? extends Void>>(RestResult.fail(Void.class).addError(exception.getMessage()),
        HttpStatus.NOT_ACCEPTABLE);
  }

  @ExceptionHandler({PaymentException.class})
  @ResponseBody
  public ResponseEntity<RestResult<? extends Void>> handlePaymentException(PaymentException exception, HttpServletRequest request) {

    return new ResponseEntity<>(RestResult.fail(Void.class).addError(exception.getMessage()),
        HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler({PaymentInvalidParameterException.class})
  @ResponseBody
  public ResponseEntity<RestResult<? extends Void>> handleIllegalException(PaymentInvalidParameterException exception, HttpServletRequest request) {

    return new ResponseEntity<RestResult<? extends Void>>(
        RestResult.fail(Void.class).addError(CollectionUtils.isEmpty(exception.getMessages()) ? exception.getMessage() : exception.getMessages().get(0)),
        HttpStatus.BAD_REQUEST);
  }

  /**
   * Wrong INPUT data : missing required data, bad format data (rest controller level) Return http status 400 - BAD REQUEST
   */
  @ExceptionHandler({ServletException.class, HttpMessageNotReadableException.class, MethodArgumentNotValidException.class})
  @ResponseBody
  public ResponseEntity<RestResult<Void>> handleServletException(Exception exception, HttpServletRequest request) {

    return new ResponseEntity<>(RestResult.fail(Void.class).addError(exception.getMessage()),
        HttpStatus.BAD_REQUEST);
  }

  /**
   * Wrong INPUT data : missing required data, bad format data (service level) Return http status 400 - BAD REQUEST
   *
   * @author ha@infinitechnology.com
   * @date Apr 19, 2017
   */
  @ExceptionHandler({IllegalArgumentException.class})
  @ResponseBody
  public ResponseEntity<RestResult<Void>> handleIllegalException(IllegalArgumentException exception, HttpServletRequest request) {

    return new ResponseEntity<>(RestResult.fail(Void.class).addError(exception.getMessage()),
        HttpStatus.BAD_REQUEST);
  }

  /**
   * Wrong Multipart file Return http status 400 - BAD REQUEST
   *
   * @author Tung Do
   * @date Apr 19, 2017
   */
  @ExceptionHandler({MaxUploadSizeExceededException.class})
  @ResponseBody
  public ResponseEntity<RestResult<Void>> handleFileException(MultipartException exception, HttpServletRequest request) {

    return new ResponseEntity<>(RestResult.fail(Void.class).addError(exception.getMessage()),
        HttpStatus.BAD_REQUEST);
  }

  /**
   * Unexpected runtime Exception Return http status 500 - INTERNAL_SERVER_ERROR
   */
  @ExceptionHandler({RuntimeException.class})
  @ResponseBody
  public ResponseEntity<RestResult<Void>> handleRuntimeException(RuntimeException exception, HttpServletRequest request) {
    return handleException(exception, request);
  }

  /**
   * Method not implemented Return http status 501 - NOT_IMPLEMENTED
   *
   * @author vu@infinitechnology.com
   * @date July 21, 2017
   */
  @ExceptionHandler({NotImplementedException.class})
  @ResponseBody
  public ResponseEntity<RestResult<Void>> handleNotImplementedException(Exception exception, HttpServletRequest request) {

    List<String> errors = new ArrayList<>();
    errors.add("This method not implemented yet");
    RestResult<Void> errorResponse = RestResult.create(Void.class)
        .setStatus(RestResult.STATUS_ERROR)
        .setMessages(getRecMsgErrors(exception, errors));
    return new ResponseEntity<>(errorResponse, HttpStatus.NOT_IMPLEMENTED);
  }

  /**
   * Final exception handler Return http status 500 - INTERNAL_SERVER_ERROR
   *
   * @author ha@infinitechnology.com
   * @date Apr 19, 2017
   */
  @ExceptionHandler({Exception.class})
  @ResponseBody
  public ResponseEntity<RestResult<Void>> handleException(Exception exception, HttpServletRequest request) {

    List<String> errors = new ArrayList<>();
    errors.add("Unexpected errors occured");
    RestResult<Void> errorResponse = RestResult.create(Void.class)
        .setStatus(RestResult.STATUS_ERROR)
        .setMessages(getRecMsgErrors(exception, errors));
    return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  public <T, V> V convertEntityToDTO(T entity, Class<V> dtoClass)
      throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
    V dto = dtoClass.getDeclaredConstructor().newInstance();
    BeanUtils.copyProperties(entity, dto);
    return dto;
  }

  protected ResponseEntity<RestResult> createErrorResponse(HttpStatus httpStatus, List<String> messages, Object data) {
    RestResult result = new RestResult();
    result.setStatus(RestResult.STATUS_ERROR).setData(data);
    for (String message : messages) {
      result.addMessage(message);
    }
    return new ResponseEntity<>(result, httpStatus);
  }

  private List<String> getRecMsgErrors(Throwable e, List<String> errors) {
    if (errors == null) {
      errors = new LinkedList<String>();
    }
    errors.add(e.getMessage());
    if (e.getCause() != null) {
      getRecMsgErrors(e.getCause(), errors);
    }
    return errors;
  }

  private ResponseEntity<RestResult<Object>> createExceptionResponse(HttpStatus httpStatus, List<String> messages, List<String> data) {
    RestResult<Object> result = new RestResult<>();
    result.setStatus(RestResult.STATUS_ERROR).setData(data);
    for (String message : messages) {
      result.addMessage(message);
    }
    return new ResponseEntity<>(result, httpStatus);
  }

  protected Double toDoubleFromString(String number) {
    if (StringUtils.isEmpty(number)) {
      return null;
    }
    try {
      return Double.valueOf(number);
    } catch (NumberFormatException e) {

      return null;
    }
  }

  protected Long toLongFromString(String number) {
    if (StringUtils.isEmpty(number)) {
      return null;
    }
    try {
      return Long.valueOf(number);
    } catch (NumberFormatException e) {

      return null;
    }
  }
}
