package com.miga.controller;

import com.miga.business.NewsTemplateBusiness;
import com.miga.common.request.NewsTemplateRequest;
import com.miga.result.RestResult;
import com.miga.result.newsTemplate.NewsTemplateDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/news-template")
public class NewsTemplateController extends BaseController {

  @Autowired
  NewsTemplateBusiness newsTemplateBusiness;

  @PostMapping()
  public ResponseEntity<RestResult<NewsTemplateDTO>> createNewsTemplate(@RequestBody NewsTemplateRequest request) {
    RestResult<NewsTemplateDTO> result = new RestResult<>();
    result.setData(newsTemplateBusiness.createNewsTemplate(request));
    result.ok("Create template success");
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @GetMapping()
  public ResponseEntity<RestResult<List<NewsTemplateDTO>>> getNewsTemplate() {
    RestResult<List<NewsTemplateDTO>> result = new RestResult<>();
    result.setData(newsTemplateBusiness.getNewsTemplate(null));
    result.ok("Create template success");
    return new ResponseEntity<>(result, HttpStatus.OK);
  }
}
