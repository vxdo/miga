package com.miga.controller;

import com.miga.business.RecyclingLocationBusiness;
import com.miga.request.LocationRequest;
import com.miga.result.RestResult;
import com.miga.result.recyclingLocation.LocationDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/recycling-location")
public class RecyclingLocationController extends BaseController {

  @Autowired
  private RecyclingLocationBusiness recyclingLocationBusiness;

  @GetMapping()
  public ResponseEntity<RestResult<List<LocationDTO>>> getLocations() {
    RestResult<List<LocationDTO>> result = new RestResult<>();
    result.setData(recyclingLocationBusiness.getLocations(null));
    result.ok("Search location success");
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping()
  public ResponseEntity<RestResult<LocationDTO>> createNewsTemplate(@RequestBody LocationRequest request) {
    RestResult<LocationDTO> result = new RestResult<>();
    result.setData(recyclingLocationBusiness.createLocation(request));
    result.ok("Create location success");
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

}
