package com.miga.controller;

import com.miga.business.SaleTicketBusiness;
import com.miga.request.SaleTicketRequest;
import com.miga.result.RestResult;
import com.miga.result.SaleTicket.SaleTicketDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sale-ticket")
public class SaleTicketController extends BaseController {

  @Autowired
  private SaleTicketBusiness saleTicketBusiness;

  @PostMapping()
  public ResponseEntity<RestResult<SaleTicketDTO>> createTicket(@RequestBody SaleTicketRequest request) {
    RestResult<SaleTicketDTO> result = new RestResult<>();
    result.setData(saleTicketBusiness.createTicket(request));
    result.ok("Create ticket success");
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @GetMapping()
  public ResponseEntity<RestResult<List<SaleTicketDTO>>> getTickets(
      @RequestParam(name = "offset", required = false) Long limit,
      @RequestParam(name = "offset", required = false) Long offset
  ) {
    RestResult<List<SaleTicketDTO>> result = new RestResult<>();
    SaleTicketRequest request = new SaleTicketRequest();
    if (limit != null) {
      request.setLimit(limit);
    }
    if (offset != null) {
      request.setOffset(offset);
    }
    result.setData(saleTicketBusiness.search(request));
    result.addMetaData("total", "" + result.getData().size());
    result.ok("get ticket success");
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PutMapping()
  public ResponseEntity<RestResult<SaleTicketDTO>> updateTicket(@RequestBody SaleTicketRequest request) {
    RestResult<SaleTicketDTO> result = new RestResult<>();
    result.setData(saleTicketBusiness.createTicket(request));
    result.ok("update ticket success");
    return new ResponseEntity<>(result, HttpStatus.OK);
  }
}
