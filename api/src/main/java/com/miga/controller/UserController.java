package com.miga.controller;

import com.google.gson.Gson;
import com.miga.business.UserBusiness;
import com.miga.entity.CustomUserDetails;
import com.miga.entity.User;
import com.miga.mapper.OAuth2AccessTokenMapper;
import com.miga.request.ChangePasswordRequest;
import com.miga.request.CustomOAuth2Request;
import com.miga.request.ForgotPasswordRequest;
import com.miga.request.UserRequest;
import com.miga.result.RestResult;
import com.miga.result.profile.OAuth2AccessTokenDTO;
import com.miga.result.profile.UserDTO;
import com.miga.service.ConnectingUserService;
import com.miga.service.UserService;
import com.miga.util.PasswordUtils;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

  @Autowired
  UserBusiness userBusiness;

  @Autowired
  UserService userService;

  @Autowired
  ConnectingUserService connectingUserService;

  @GetMapping("/profile")
  public ResponseEntity<RestResult<UserDTO>> getCurrentUser() {
    RestResult<UserDTO> result = new RestResult<>();
    result.setData(userBusiness.findUserByUserId(connectingUserService.getConnectingUser().getUserId()));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping(consumes = "multipart/form-data")
  public ResponseEntity<RestResult<UserDTO>> registerUser(
      @RequestPart("request") String request, @RequestPart(name = "avatar", required = false) MultipartFile avatar) throws Exception {
    RestResult<UserDTO> result = new RestResult<>();
    UserRequest userRequest = new Gson().fromJson(request, UserRequest.class);
    userRequest.setAvatar(avatar);
    result.setData(userBusiness.register(userRequest, avatar));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping("/login")
  public ResponseEntity<RestResult<OAuth2AccessTokenDTO>> login(HttpServletRequest httpRequest, @RequestBody CustomOAuth2Request request)
      throws HttpRequestMethodNotSupportedException {
    request.setLanguage(httpRequest.getHeader(HttpHeaders.ACCEPT_LANGUAGE));
    OAuth2AccessTokenDTO token = OAuth2AccessTokenMapper.INSTANCE.toOAuth2AccessTokenDTO(connectingUserService.login(request));
    RestResult<OAuth2AccessTokenDTO> result = RestResult.ok(OAuth2AccessTokenDTO.class);
    result.setData(token);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping("/logout")
  public ResponseEntity<RestResult> logout() {
    connectingUserService.logout();
    RestResult result = RestResult.ok(Void.class);
    result.addMessage("User logout");
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping("/forgot")
  public ResponseEntity<RestResult> resetPassword(@RequestBody ForgotPasswordRequest forgotPasswordRequest) {
    try {
      RestResult result = RestResult.ok(Void.class);
      userBusiness.resetPassword(forgotPasswordRequest.getEmail());
      result.addMessage("Reset password success");
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      RestResult result = RestResult.fail(Void.class);
      result.addMessage(e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
    }
  }

  @PutMapping("/password")
  public ResponseEntity<RestResult> changePassword(@Validated @RequestBody ChangePasswordRequest request, BindingResult bindingResult) {
    CustomUserDetails userConnect = connectingUserService.getConnectingUser();
    if (userConnect.getUserId() != null) {
      User user = userService.findByUserId(userConnect.getUserId());
      user.setPassword(PasswordUtils.encrypt(request.getNewPassword()));
      userBusiness.save(user);
    } else {
      throw new IllegalArgumentException("Must login to change password");
    }
    return new ResponseEntity<>(RestResult.ok(Void.class, "Change password successfully", null), HttpStatus.OK);
  }
}
