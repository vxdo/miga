package com.miga.mapper;

import java.sql.Timestamp;
import org.mapstruct.Mapper;

@Mapper
public interface BaseMapper {

  default Long TimestampToLong(Timestamp ts) {
    return ts.getTime();
  }
}
