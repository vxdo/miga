package com.miga.mapper;

import com.miga.entity.RecyclingLocation;
import com.miga.result.recyclingLocation.LocationDTO;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface LocationMapper extends BaseMapper {

  LocationMapper INSTANCE = Mappers.getMapper(LocationMapper.class);

  LocationDTO toLocationDTO(RecyclingLocation location);

  List<LocationDTO> toLocationDTOs(List<RecyclingLocation> locations);
}
