package com.miga.mapper;

import com.miga.entity.NewsTemplate;
import com.miga.result.newsTemplate.NewsTemplateDTO;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper()
public interface NewsTemplateMapper extends BaseMapper {

  NewsTemplateMapper INSTANCE = Mappers.getMapper(NewsTemplateMapper.class);

  NewsTemplateDTO toNewsTemplateDTO(NewsTemplate template);

  List<NewsTemplateDTO> toNewsTemplateDTOs(List<NewsTemplate> templates);


}
