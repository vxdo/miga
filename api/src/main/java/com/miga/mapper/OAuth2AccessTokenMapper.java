package com.miga.mapper;

import com.miga.result.profile.OAuth2AccessTokenDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

@Mapper
public interface OAuth2AccessTokenMapper extends BaseMapper {

  OAuth2AccessTokenMapper INSTANCE = Mappers.getMapper(OAuth2AccessTokenMapper.class);

  default OAuth2AccessTokenDTO toOAuth2AccessTokenDTO(OAuth2AccessToken token) {
    OAuth2AccessTokenDTO dto = new OAuth2AccessTokenDTO();
    dto.setAccessToken(token.getValue());
    dto.setRefreshToken(token.getRefreshToken().getValue());
    dto.setExpiresIn(token.getExpiresIn());
    dto.setTokenType(OAuth2AccessToken.BEARER_TYPE);
    dto.setScope(token.getScope());
    return dto;
  }

}
