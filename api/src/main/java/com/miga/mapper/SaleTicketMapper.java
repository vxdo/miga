package com.miga.mapper;

import com.miga.entity.SaleTicket;
import com.miga.result.SaleTicket.SaleTicketDTO;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SaleTicketMapper extends BaseMapper {

  SaleTicketMapper INSTANCE = Mappers.getMapper(SaleTicketMapper.class);

  SaleTicketDTO toSaleTicketDTO(SaleTicket ticket);

  List<SaleTicketDTO> toSaleTicketDTOs(List<SaleTicket> tickets);
}
