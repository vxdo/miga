package com.miga.mapper;

import com.miga.entity.User;
import com.miga.result.profile.UserDTO;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper extends BaseMapper {

  UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

  UserDTO toUserDTO(User user);

  List<UserDTO> toUserDTOs(List<User> user);
}
