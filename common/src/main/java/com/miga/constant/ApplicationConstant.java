package com.miga.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationConstant {

  public static final String SECRET_KEY = "thispasswordisuncrackableHahwhahawh162?@%";

  public static String ROOT_STORAGE_PATH;
  public static String DOMAIN_PATH;

  @Value("${storage.rootPath}")
  public void setRootStoragePath(String rootStoragePath) {
    ROOT_STORAGE_PATH = rootStoragePath;
  }

  @Value("${storage.domainPath}")
  public void setDomainPath(String domainPath) {
    DOMAIN_PATH = domainPath;
  }
}
