package com.miga.enumeration.profile;

public enum AttachmentType {
  AVATAR("avatar", "System avatar file"),
  PHOTO("photo", "System photo file"),
  PRICING("pricing", "System pricing file"),
  BILL("bill", "System bill file");

  String folder;
  String description;

  AttachmentType(String value, String description) {
    this.folder = value;
    this.description = description;
  }

  public String getFolder() {
    return folder;
  }

  public String getDescription() {
    return description;
  }
}
