package com.miga.enumeration.profile;

/**
 * Created by chautn on 7/28/2017.
 */
public enum UserStatus {
  ACTIVE, // user
  INACTIVE, // user
  INIT, // actor
  REGISTERED, // actor
  AVAILABLE, // provider
  INVISIBLE;  // provider

  public static UserStatus getUserStatus(String status) {
    for (UserStatus userStatus : values()) {
      if (userStatus.name().compareTo(status) == 0) {
        return userStatus;
      }
    }
    return null;
  }
}
