package com.miga.enumeration.profile;

public enum UserType {
  USER("MU_", "user_id_generator_seq", "Generate id with MU_ as prefix", "not_use", false),
  CONSUMER("CO_", "consumer_id_generator_seq", "Generate id with CO_ as prefix", "miga_consumer", true),
  PROVIDER("PR_", "provider_id_generator_seq", "Generate id with PR_ as prefix", "miga_provider", true),
  CMS("not_use", "not_use", "Authenticated directly on table m_user", "miga_cms", true),
  ANONYMOUS("ANONYMOUS", "ANONYMOUS", "Anonymous user is only available in test mode", "ANONYMOUS", false);

  private String value;
  private String sequence;
  private String description;
  private String client;
  private boolean baseGroup;

  UserType(String value, String sequence, String description, String client, boolean baseGroup) {
    this.value = value;
    this.sequence = sequence;
    this.description = description;
    this.client = client;
    this.baseGroup = baseGroup;
  }

  public static UserType getUserType(String type) {
    for (UserType userType : UserType.values()) {
      if (userType.name().equalsIgnoreCase(type)) {
        return userType;
      }
    }
    return null;
  }

  public static UserType getUserTypeByClient(String client) {
    for (UserType userType : UserType.values()) {
      if (userType.client().equalsIgnoreCase(client)) {
        return userType;
      }
    }
    return null;
  }

  public String value() {
    return value;
  }

  public String description() {
    return description;
  }

  public String sequence() {
    return sequence;
  }

  public String client() {
    return client;
  }

  public boolean isBaseGroup() {
    return baseGroup;
  }
}
