package com.miga.exception;

import lombok.Getter;

public class ApacheClientException extends RuntimeException {

  @Getter
  private String code;

  public ApacheClientException(String message) {
    super(message);
  }

  public ApacheClientException(String message, String code) {
    super(message);
    this.code = code;
  }
}
