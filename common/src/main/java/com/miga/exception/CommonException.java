package com.miga.exception;

public class CommonException extends Exception {

  private static final long serialVersionUID = -4962181426322090385L;

  public CommonException(String message) {
    super(message);
  }

  public CommonException(String message, Exception e) {
    super(message, e);
  }
}
