package com.miga.exception;

import java.util.List;

public class CustomInvalidParameterException extends Throwable {

  private List<String> messages;

  public CustomInvalidParameterException(List<String> messages) {
    this.messages = messages;
  }

  public CustomInvalidParameterException(String msg) {
    super(msg);
  }

  public List<String> getMessages() {
    return messages;
  }

  public void setMessages(List<String> messages) {
    this.messages = messages;
  }
}
