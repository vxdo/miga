package com.miga.exception;

public class PaymentException extends Exception {

  private static final long serialVersionUID = -4962181426322090385L;

  public PaymentException(String message) {
    super(message);
  }

  public PaymentException(String message, Exception e) {
    super(message, e);
  }
}
