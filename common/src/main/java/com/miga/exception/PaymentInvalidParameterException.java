package com.miga.exception;

import java.security.InvalidParameterException;
import java.util.List;
import lombok.Data;

@Data
public class PaymentInvalidParameterException extends InvalidParameterException {

  private List<String> messages;

  public PaymentInvalidParameterException(List<String> messages) {
    this.messages = messages;
  }

  public PaymentInvalidParameterException(String message) {
    super(message);
  }
}
