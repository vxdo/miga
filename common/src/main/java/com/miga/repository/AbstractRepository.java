package com.miga.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

public class AbstractRepository<T> {

  @PersistenceContext
  protected EntityManager entityManager;

  public Query query(String sql) {
    return entityManager.createQuery(sql);
  }

  public TypedQuery<T> typedQuery(String sql, Class<T> c) {
    return entityManager.createQuery(sql, c);
  }

  public Query nativeQuery(String sql, Class<T> c) {
    return entityManager.createNativeQuery(sql, c);
  }

}
