package com.miga.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomOAuth2Request {

  private String clientId;
  private String grantType;
  private String username;
  private String password;
  private String phone;
  private String email;
  private String language;
}
