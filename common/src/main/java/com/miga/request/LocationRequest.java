package com.miga.request;

import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocationRequest {

  private Long locationId;
  private String name;
  private String address;
  private BigDecimal longitude;
  private BigDecimal latitude;
  private String type;
  private String collectingType;
  private String note;
}
