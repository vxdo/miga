package com.miga.common.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewsTemplateRequest {

  private Long userId;

  private String templateData;
}
