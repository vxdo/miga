package com.miga.request;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class SaleTicketRequest {

  private Long saleTicketId;
  private String productName;
  private BigDecimal price;
  private String status;
  private Long createdDate;
  private Long createdBy;
  private Long modifiedDate;
  private Long modifiedBy;
  private String description;
  private Long limit = 10L;
  private Long offset = 0L;
}
