package com.miga.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class UserRequest {

  private Long userId;

  private String username;

  private String password;

  private String email;

  private String phone;

  private String fullName;

  private String firstName;

  private String middleName;

  private String lastName;

  private String dob;

  private String gender;

  private String address;

  private String city;

  private String state;

  private String country;

  private String zipCode;

  private MultipartFile avatar;
}
