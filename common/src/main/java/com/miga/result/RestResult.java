package com.miga.result;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author VuLD
 * @date Dec 27, 2016
 */
@Getter
@Setter
public class RestResult<T> {

  public static final String STATUS_SUCCESS = "success";
  public static final String STATUS_ERROR = "error";

  @JsonView(Object.class)
  private String status;

  @JsonView(Object.class)
  private List<String> messages;

  private String message;

  @JsonView(Object.class)
  private T data;

  private HashMap<String, String> metaData;

  /**
   * Constructor
   *
   * @author VuLD
   * @date Dec 27, 2016
   */
  public RestResult() {
    messages = new ArrayList<>();
    metaData = new HashMap<>();
    status = STATUS_SUCCESS;
  }

  /**
   * Create new RestResult object
   *
   * @author VuLD
   * @date Dec 27, 2016
   */
  public static <T> RestResult<T> create(Class<T> genericType) {
    return new RestResult<T>();
  }

  /**
   * Create new RestResult object with status success
   *
   * @author VuLD
   * @date Dec 27, 2016
   */
  public static <T> RestResult<T> ok(Class<T> type) {
    RestResult<T> result = new RestResult<T>();
    result.status = STATUS_SUCCESS;
    return result;
  }

  /**
   * Create new RestResult object with status success
   *
   * @author VuLD
   * @date Dec 27, 2016
   */
  public static <T> RestResult<T> ok(Class<T> type, String message, T data) {
    RestResult<T> result = new RestResult<T>();
    result.status = STATUS_SUCCESS;
    result.addMessage(message);
    result.data = data;
    return result;
  }

  /**
   * Create new RestResult object with status error
   *
   * @author VuLD
   * @date Dec 27, 2016
   */
  public static <T> RestResult<T> fail(Class<T> type) {
    RestResult<T> result = new RestResult<T>();
    result.status = STATUS_ERROR;
    return result;
  }

  public static <T> T getRestResultData(Object object, Class<T> clazz) {
    return new GsonBuilder().create().fromJson(new GsonBuilder().create().toJson(object), clazz);
  }

  public static <T> List<T> getRestResultListData(Object object, Class<T[]> clazz) {
    return Arrays.asList(new Gson().fromJson(new GsonBuilder().create().toJson(object), clazz));
  }

  /**
   * set object RestResult to success and add message
   *
   * @author VuLD
   * @date Dec 27, 2016
   */
  public void ok(String message) {
    this.status = STATUS_SUCCESS;
    addMessage(message);
  }

  /**
   * set object RestResult to success and add message
   *
   * @author VuLD
   * @date Dec 27, 2016
   */
  public void ok(String message, T data) {
    this.status = STATUS_SUCCESS;
    addMessage(message);
    this.data = data;
  }

  public T getData() {
    return data;
  }

  public RestResult<T> setData(T data) {
    this.data = data;
    return this;
  }

  /**
   * @author VuLD
   * @date Dec 27, 2016
   */
  public void fail(String message) {
    this.status = STATUS_ERROR;
    addMessage(message);
  }

  /**
   * Getter & Setter
   */
  public String getStatus() {
    return status;
  }

  public RestResult<T> setStatus(String status) {
    this.status = status;
    return this;
  }

  public List<String> getMessages() {
    return messages;
  }

  public RestResult<T> setMessages(List<String> messages) {
    this.messages.addAll(messages);
    return this;
  }

  public void addMessage(String message) {
    messages.add(message);
  }

  public RestResult<T> addError(String error) {
    this.status = STATUS_ERROR;
    this.addMessage(error);
    return this;
  }

  @JsonIgnore
  public boolean isError() {
    return !StringUtils.equals(STATUS_SUCCESS, status);
  }

  public String getMessage() {
    if (StringUtils.isEmpty(message) && CollectionUtils.isNotEmpty(messages)) {
      message = String.join(",", this.messages);
    }
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }


  public HashMap<String, String> getMetaData() {
    return metaData;
  }

  public void setMetaData(HashMap<String, String> metaData) {
    this.metaData = metaData;
  }

  public void addMetaData(String key, String value) {
    this.metaData.put(key, value);
  }

}
