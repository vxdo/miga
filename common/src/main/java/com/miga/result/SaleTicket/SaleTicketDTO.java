package com.miga.result.SaleTicket;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class SaleTicketDTO {

  private Long saleTicketId;
  private Long userId;
  private String product_name;
  private BigDecimal price;
  private String description;
  private String status;
  private Long createdDate;
  private Long createdBy;
  private Long modifiedDate;
  private Long modifiedBy;

}
