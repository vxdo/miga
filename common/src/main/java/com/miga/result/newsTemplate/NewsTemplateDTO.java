package com.miga.result.newsTemplate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewsTemplateDTO {

  private Long newsTemplateId;

  private Long userId;

  private String newsTemplateData;

  private Long createdDate;

  private Long modifiedDate;

  private String createdBy;

  private String modifiedBy;

}
