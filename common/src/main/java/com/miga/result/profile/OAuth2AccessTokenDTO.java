package com.miga.result.profile;

import java.util.Set;
import lombok.Data;

@Data
public class OAuth2AccessTokenDTO {

  private String accessToken;
  private String refreshToken;
  private String tokenType;
  private long expiresIn;
  private Set<String> scope;

}
