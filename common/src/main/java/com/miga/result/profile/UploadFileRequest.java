package com.miga.result.profile;

import java.io.InputStream;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class UploadFileRequest {

  private MultipartFile attachment;
  private InputStream fileInputStream;
  private String fileName;
  private String fileType;
  private String fileExtensionType;
}
