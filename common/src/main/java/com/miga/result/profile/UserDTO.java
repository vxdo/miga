package com.miga.result.profile;

import com.miga.result.newsTemplate.NewsTemplateDTO;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDTO {

  private Long userId;

  private String userName;

  private String password;

  private String email;

  private String phone;

  private String fullName;

  private String firstName;

  private String middleName;

  private String lastName;

  private String gender;

  private String address;

  private String city;

  private String state;

  private String country;

  private String zipCode;

  private Set<NewsTemplateDTO> template;
}
