package com.miga.result.recyclingLocation;

import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocationDTO {

  private Long locationId;
  private String name;
  private String address;
  private BigDecimal longitude;
  private BigDecimal latitude;
  private String createdBy;
  private Long createdDate;
}
