package com.miga.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import org.apache.commons.lang3.time.DateUtils;

public class DateTimeUtils extends DateUtils {

  // FOR MIGA
  public static final String DD_MM_YYYY = "dd/MM/yyyy";
  public static final String DD_MM_YY_HH_MM_SS = "dd/MM/yy HH:mm:ss";

  // FOR IMPORT
  public static final String MM_DD_YY_HH_MM = "MM/dd/yy HH:mm";

  public static final String HH_MM_SS = "HH:mm:ss";

  public static final String HH_MM = "HH:mm";

//  public static String[] datePatterns() {
//    return new String[]{DD_MM_YYYY, YYYY_MM_DD};
//  }

  public static Timestamp nowTimestamp() {
    return new Timestamp(Calendar.getInstance().getTimeInMillis());
  }

  public static Date nowDate() {
    return Calendar.getInstance().getTime();
  }

  public static int compareDate(Date date1, Date date2) {
    if (date1 == null || date2 == null) {
      throw new NullPointerException();
    } else {
      Calendar calendar1 = Calendar.getInstance();
      Calendar calendar2 = Calendar.getInstance();
      calendar1.setTime(date1);
      calendar2.setTime(date2);
      // compare year
      int year1 = calendar1.get(Calendar.YEAR);
      int year2 = calendar2.get(Calendar.YEAR);
      if (year1 > year2) {
        return 1;
      } else if (year1 < year2) {
        return -1;
      } else {
        // compare month
        int month1 = calendar1.get(Calendar.MONTH);
        int month2 = calendar2.get(Calendar.MONTH);
        if (month1 > month2) {
          return 1;
        } else if (month1 < month2) {
          return -1;
        } else {
          // compare day
          int day1 = calendar1.get(Calendar.DAY_OF_MONTH);
          int day2 = calendar2.get(Calendar.DAY_OF_MONTH);
          if (day1 > day2) {
            return 1;
          } else if (day1 < day2) {
            return -1;
          } else {
            return 0;
          }
        }
      }
    }
  }

  public static Date endOfDay() {
    return endOfDate(Calendar.getInstance().getTime());
  }

  public static Date endOfDate(Date date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.set(Calendar.HOUR_OF_DAY, 23);
    calendar.set(Calendar.MINUTE, 59);
    calendar.set(Calendar.SECOND, 59);
    calendar.set(Calendar.MILLISECOND, 999);
    return calendar.getTime();
  }

  public static Date beginOfDay() {
    return beginOfDate(Calendar.getInstance().getTime());
  }

  public static Date beginOfDate(Date date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    return calendar.getTime();
  }

  public static String formatDate(Date date, String format) {
    return new SimpleDateFormat(format).format(date);
  }

  public static String formatDate(LocalDate date, String format) {
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
    return date.format(dateTimeFormatter);
  }

  public static LocalDate toLocalDate(Instant startTime) {
    return LocalDateTime.ofInstant(startTime, ZoneId.of("GMT+7")).toLocalDate();
  }
}

