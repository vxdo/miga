package com.miga.util;

import org.springframework.security.crypto.bcrypt.BCrypt;

public class PasswordUtils {

  public static String encrypt(String string) {
    return BCrypt.hashpw(string, BCrypt.gensalt(16));
  }

  public static boolean checkPassword(String plain, String encrypted) {
    return BCrypt.checkpw(plain, encrypted);
  }
}
