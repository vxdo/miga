package com.miga.entity;

import com.miga.enumeration.profile.UserType;
import com.miga.util.PasswordUtils;
import java.io.Serializable;
import java.security.Principal;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomUserDetails implements Principal, Serializable {

  private UserType userType;
  private Long userId;
  private String actorId;
  private String deviceToken;

  public boolean isAnonymous() {
    return userType == null || UserType.ANONYMOUS.equals(userType);
  }

  @Override
  public String getName() {
    try {
      return actorId + "_" + PasswordUtils.encrypt(deviceToken == null ? "" : deviceToken);
    } catch (Exception e) {
      throw new IllegalArgumentException("Something is wrong when encrypting" + e);
    }
  }
}
