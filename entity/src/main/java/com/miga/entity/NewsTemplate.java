package com.miga.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.miga.listener.CUEntity;
import com.miga.listener.CUEntityListener;
import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "news_template")
@Getter
@Setter
@EntityListeners(CUEntityListener.class)
public class NewsTemplate implements Serializable, CUEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "news_template_id", unique = true, nullable = false)
  private Long newsTemplateId;

  @Column(name = "template_data")
  private String newsTemplateData;

  @Column(name = "created_date")
  private Timestamp createdDate;

  @Column(name = "created_by")
  private String createdBy;

  @Column(name = "modified_date")
  private Timestamp modifiedDate;

  @Column(name = "modified_by")
  private String modifiedBy;

  @Column(name = "user_id", insertable = false, updatable = false)
  private Long userId;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id", referencedColumnName = "user_id")
  @JsonManagedReference
  private User user;

}
