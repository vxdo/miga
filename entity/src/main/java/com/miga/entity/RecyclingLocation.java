package com.miga.entity;

import com.miga.listener.CUEntity;
import com.miga.listener.CUEntityListener;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "recycling_location")
@Getter
@Setter
@EntityListeners(CUEntityListener.class)
public class RecyclingLocation implements Serializable, CUEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "location_id", unique = true, nullable = false)
  private Long locationId;

  @Column(name = "name")
  private String name;

  @Column(name = "address")
  private String address;

  @Column(name = "longitude", nullable = false)
  private BigDecimal longitude;

  @Column(name = "latitude", nullable = false)
  private BigDecimal latitude;

  @Column(name = "created_date")
  private Timestamp createdDate;

  @Column(name = "created_by")
  private String createdBy;

  @Column(name = "modified_date")
  private Timestamp modifiedDate;

  @Column(name = "modified_by")
  private String modifiedBy;

  @Column(name = "type")
  private String type;

  @Column(name = "collecting_type")
  private String collectingType;

  @Column(name = "note")
  private String note;
}
