package com.miga.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.miga.listener.CUEntityListener;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "sale_ticket")
@Getter
@Setter
@EntityListeners(CUEntityListener.class)
public class SaleTicket implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "sale_ticket_id", unique = true, nullable = false)
  private Long locationId;

  @Column(name = "user_id", insertable = false, updatable = false)
  private Long userId;

  @Column(name = "product_name")
  private String productName;

  @Column(name = "price")
  private BigDecimal price;

  @Column(name = "status")
  private String status;

  @Column(name = "description")
  private String description;

  @Column(name = "created_by")
  private String createdBy;

  @Column(name = "created_date")
  private Timestamp createdDate;

  @Column(name = "modified_date")
  private Timestamp modifiedDate;

  @Column(name = "modified_by")
  private String modifiedBy;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id", referencedColumnName = "user_id", insertable = false, nullable = false)
  @JsonManagedReference
  private User user;

}
