package com.miga.entity.security;

import org.springframework.security.core.GrantedAuthority;

public class GrantedFunction implements GrantedAuthority {

  private String authority;

  public GrantedFunction(String authority) {
    this.authority = authority;
  }

  @Override
  public String getAuthority() {
    return authority;
  }
}
