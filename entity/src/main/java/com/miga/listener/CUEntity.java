package com.miga.listener;

import java.sql.Timestamp;

public interface CUEntity {

  void setModifiedDate(Timestamp modifiedDate);

  void setModifiedBy(String modifiedBy);

  void setCreatedBy(String createdBy);

  void setCreatedDate(Timestamp createdDate);
}
