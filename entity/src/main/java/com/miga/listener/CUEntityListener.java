package com.miga.listener;

import com.miga.entity.CustomUserDetails;
import com.miga.enumeration.profile.UserType;
import java.sql.Timestamp;
import java.util.Calendar;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class CUEntityListener {

  public String getActorId() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null && auth.getPrincipal() != null && auth.getPrincipal() instanceof CustomUserDetails) {
      CustomUserDetails customUserDetails = (CustomUserDetails) auth.getPrincipal();
      return customUserDetails.getActorId();
    } else {
      return UserType.ANONYMOUS.client();
    }
  }

  @PrePersist
  public void onCreate(CUEntity entity) {
    entity.setCreatedBy(getActorId());
    entity.setCreatedDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
    entity.setModifiedBy(getActorId());
    entity.setModifiedDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
  }

  @PreUpdate
  public void onUpdate(CUEntity entity) {
    entity.setModifiedBy(getActorId());
    entity.setModifiedDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
  }
}
