package com.miga.repository;

import com.miga.entity.NewsTemplate;
import com.miga.repository.custom.NewsTemplateRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NewsTemplateRepository extends JpaRepository<NewsTemplate, Long>, NewsTemplateRepositoryCustom {

}
