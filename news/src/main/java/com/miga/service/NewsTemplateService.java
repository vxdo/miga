package com.miga.service;

import com.miga.common.request.NewsTemplateRequest;
import com.miga.entity.NewsTemplate;
import java.util.List;

public interface NewsTemplateService {

  NewsTemplate createNewsTemplate(NewsTemplateRequest request);

  List<NewsTemplate> getNewsTemplate(NewsTemplateRequest request);
}
