package com.miga.service.impl;

import com.miga.common.request.NewsTemplateRequest;
import com.miga.entity.NewsTemplate;
import com.miga.entity.User;
import com.miga.repository.NewsTemplateRepository;
import com.miga.service.NewsTemplateService;
import com.miga.service.UserService;
import com.miga.util.DateTimeUtils;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NewsTemplateServiceImpl implements NewsTemplateService {

  @Autowired
  NewsTemplateRepository newsTemplateRepository;

  @Autowired
  UserService userService;

  @Override
  public NewsTemplate createNewsTemplate(NewsTemplateRequest request) {
    NewsTemplate newsTemplate = new NewsTemplate();
    User user = userService.findByUserId(request.getUserId());
    if (user == null) {
      throw new IllegalArgumentException("User id doesn't exist");
    }
    if (StringUtils.isBlank(request.getTemplateData())) {
      throw new IllegalArgumentException("No data");
    }
    newsTemplate.setNewsTemplateData(request.getTemplateData());
    newsTemplate.setUserId(request.getUserId());
    newsTemplate.setUser(user);
    newsTemplate.setCreatedBy(user.getFullName());
    newsTemplate.setModifiedBy(user.getFullName());
    newsTemplate.setCreatedDate(DateTimeUtils.nowTimestamp());
    newsTemplate.setModifiedDate(DateTimeUtils.nowTimestamp());
    return newsTemplateRepository.save(newsTemplate);
  }

  @Override
  public List<NewsTemplate> getNewsTemplate(NewsTemplateRequest request) {
    return newsTemplateRepository.findAll();
  }

}
