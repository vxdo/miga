package com.miga.repository;

import com.miga.entity.User;
import com.miga.repository.custom.UserRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, UserRepositoryCustom {

  @Query(value = "SELECT * FROM miga_user u WHERE u.user_id = ?1", nativeQuery = true)
  User findByUserId(Long userId);

  User findUserByUserName(String name);

  User findUserByPhone(String phone);
}
