package com.miga.service;

import com.miga.entity.CustomUserDetails;
import com.miga.request.CustomOAuth2Request;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.HttpRequestMethodNotSupportedException;

public interface ConnectingUserService {

  Authentication getAuthentication();

  CustomUserDetails getConnectingUser();

  boolean isAnonymous();

  OAuth2AccessToken login(CustomOAuth2Request request) throws HttpRequestMethodNotSupportedException;

  void logout();
}
