package com.miga.service;

import com.miga.entity.User;
import com.miga.request.UserRequest;

public interface UserService {

  User findByUserId(Long userId);

  User findByUsername(String name);

  User findByPhone(String phone);

  User save(User user);

  User register(UserRequest request);
}
