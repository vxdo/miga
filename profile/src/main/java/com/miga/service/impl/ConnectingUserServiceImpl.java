package com.miga.service.impl;

import com.miga.entity.CustomUserDetails;
import com.miga.enumeration.profile.UserType;
import com.miga.request.CustomOAuth2Request;
import com.miga.service.ConnectingUserService;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.stereotype.Service;
import org.springframework.web.HttpRequestMethodNotSupportedException;

@Service
public class ConnectingUserServiceImpl implements ConnectingUserService {

  @Autowired
  private TokenEndpoint tokenEndpoint;

  @Autowired
  private ConsumerTokenServices tokenServices;

  @Override
  public Authentication getAuthentication() {
    return SecurityContextHolder.getContext().getAuthentication();
  }

  @Override
  public CustomUserDetails getConnectingUser() {
    Authentication auth = getAuthentication();
    if (auth != null && auth.getPrincipal() != null && auth.getPrincipal() instanceof CustomUserDetails) {
      return (CustomUserDetails) auth.getPrincipal();
    } else {
      CustomUserDetails customUserDetails = new CustomUserDetails();
      customUserDetails.setUserType(UserType.ANONYMOUS);
      customUserDetails.setActorId(UserType.ANONYMOUS.client());
      customUserDetails.setUserId(null);
      return customUserDetails;
    }
  }

  @Override
  public boolean isAnonymous() {
    Authentication auth = getAuthentication();
    if (auth == null || auth instanceof AnonymousAuthenticationToken) {
      return true;
    } else {
      return getConnectingUser().isAnonymous();
    }
  }

  @Override
  public OAuth2AccessToken login(CustomOAuth2Request request) throws HttpRequestMethodNotSupportedException {
    Authentication auth = new AnonymousAuthenticationToken("anonymousUser", request.getClientId(),
        Collections.singletonList((GrantedAuthority) () -> "ROLE_ANONYMOUS"));
    Map<String, String> map = new HashMap<>();
    map.put("grant_type", request.getGrantType());
    map.put("client_id", request.getClientId());
    map.put("username", request.getUsername());
    map.put("password", request.getPassword());
    if (StringUtils.isNotBlank(request.getLanguage())) {
      map.put("language", request.getLanguage());
    }
    return tokenEndpoint.postAccessToken(auth, map).getBody();
  }

  @Override
  public void logout() {
    if (!isAnonymous()) {
      tokenServices.revokeToken(((OAuth2AuthenticationDetails) getAuthentication().getDetails()).getTokenValue());
    }
  }
}
