package com.miga.service.impl;

import com.miga.entity.User;
import com.miga.enumeration.profile.UserStatus;
import com.miga.repository.UserRepository;
import com.miga.request.UserRequest;
import com.miga.service.UserService;
import com.miga.util.PasswordUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  UserRepository userRepository;

  @Override
  public User findByUserId(Long userId) {
    return userRepository.findByUserId(userId);
  }

  @Override
  public User findByUsername(String name) {
    return userRepository.findUserByUserName(name);
  }

  @Override
  public User findByPhone(String phone) {
    return userRepository.findUserByPhone(phone);
  }


  @Override
  public User save(User user) {
    return userRepository.save(user);
  }

  @Override
  public User register(UserRequest request) {
    User user = new User();
    user.setUserName(request.getUsername());
    user.setFullName(request.getFirstName() + ' ' + (StringUtils.isNotBlank(request.getMiddleName()) ? request.getMiddleName() + ' ' : request.getLastName()));
    user.setFirstName(request.getFirstName());
    user.setLastName(request.getLastName());
    user.setMiddleName(request.getMiddleName());
    user.setAddress(request.getAddress());
    user.setDob(request.getDob());
    user.setEmail(request.getEmail());
    user.setGender(request.getGender());
    user.setPhone(request.getPhone());
    user.setStatus(UserStatus.ACTIVE.name());
    user.setPassword(PasswordUtils.encrypt(request.getPassword()));
    user.setAddress(request.getAddress());
    user.setCity(request.getCity());
    user.setState(request.getState());
    user.setCountry(request.getCountry());
    user.setZipCode(request.getZipCode());
    user.setTempPassword("123456");
    return userRepository.save(user);
  }
}
