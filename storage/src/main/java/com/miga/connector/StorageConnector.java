package com.miga.connector;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import org.springframework.stereotype.Component;

@Component
public interface StorageConnector {

  /**
   * upload file
   */
  String createFile(InputStream file, String filePath, String fileName) throws Exception;

  void createPhoto(InputStream file, String filePath, String fileName) throws Exception;

  void removeFile(String filePath) throws Exception;

  void cleanDirectories(String directoryPath) throws Exception;

  InputStream downloadFile(String filePath) throws FileNotFoundException, IOException;

  String getFileUrl(String filePath);
}
